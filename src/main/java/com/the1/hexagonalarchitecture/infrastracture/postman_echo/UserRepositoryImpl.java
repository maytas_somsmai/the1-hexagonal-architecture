package com.the1.hexagonalarchitecture.infrastracture.postman_echo;

import com.the1.hexagonalarchitecture.domain.User;
import com.the1.hexagonalarchitecture.domain.repository.UserRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class UserRepositoryImpl implements UserRepository {

    @Cacheable(cacheNames = "user", key = "#userId")
    @Override
    public User getUser(String userId, String randomString) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject("https://postman-echo.com/post",
                User.DataObj.builder()
                        .id(userId)
                        .firstName("John")
                        .lastName("Wick")
                        .nickName("JW-"+randomString)
                        .build(),
                User.class);
    }

}
