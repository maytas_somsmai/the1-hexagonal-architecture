package com.the1.hexagonalarchitecture.infrastracture.postman_echo;

import com.the1.hexagonalarchitecture.domain.Location;
import com.the1.hexagonalarchitecture.domain.repository.LocationRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class LocationRepositoryImpl implements LocationRepository {

    @Cacheable(cacheNames = "location", key = "#userId")
    @Override
    public Location getUserAddress(String userId, String randomString) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject("https://postman-echo.com/post",
                Location.DataObj.builder()
                        .address(String.format("%s test rd. main st. bkk", randomString))
                        .zipCode(randomString)
                        .build(),
                Location.class);
    }

}
