package com.the1.hexagonalarchitecture.domain.service;

import com.the1.hexagonalarchitecture.application.response.UserInfoResponse;

public interface UserService {

    UserInfoResponse getUserInfo(String userId);

}
