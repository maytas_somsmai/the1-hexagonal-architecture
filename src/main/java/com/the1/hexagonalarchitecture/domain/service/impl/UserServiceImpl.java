package com.the1.hexagonalarchitecture.domain.service.impl;

import com.the1.hexagonalarchitecture.application.response.UserInfoResponse;
import com.the1.hexagonalarchitecture.domain.Location;
import com.the1.hexagonalarchitecture.domain.User;
import com.the1.hexagonalarchitecture.domain.mapper.UserMapper;
import com.the1.hexagonalarchitecture.domain.repository.LocationRepository;
import com.the1.hexagonalarchitecture.domain.repository.UserRepository;
import com.the1.hexagonalarchitecture.domain.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final LocationRepository locationRepository;
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceImpl(LocationRepository locationRepository, UserRepository userRepository, UserMapper userMapper) {
        this.locationRepository = locationRepository;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserInfoResponse getUserInfo(String userId) {
        //user this random string to check that repository is return things from cache.
        String randomString = String.valueOf(Math.round(Math.random() * 1000));
        User user = userRepository.getUser(userId, randomString);
        Location location = locationRepository.getUserAddress(userId, randomString);
        return userMapper.toUserInfoResponse(user, location);
    }

}
