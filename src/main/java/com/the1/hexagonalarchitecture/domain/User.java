package com.the1.hexagonalarchitecture.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private User.DataObj data;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DataObj implements Serializable{

        private String id;

        private String firstName;

        private String nickName;

        private String lastName;

    }

}