package com.the1.hexagonalarchitecture.domain.mapper;

import com.the1.hexagonalarchitecture.application.response.UserInfoResponse;
import com.the1.hexagonalarchitecture.domain.Location;
import com.the1.hexagonalarchitecture.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserInfoResponse toUserInfoResponse(User user, Location location) {
        return UserInfoResponse.builder()
                .id(user.getData().getId())
                .displayName(user.getData().getFirstName(), user.getData().getLastName(), user.getData().getNickName())
                .address(location.getData().getAddress(), location.getData().getZipCode())
                .build();
    }

}
