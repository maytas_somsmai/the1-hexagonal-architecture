package com.the1.hexagonalarchitecture.domain.repository;

import com.the1.hexagonalarchitecture.domain.Location;

public interface LocationRepository {

    Location getUserAddress(String userId, String randomString);

}
