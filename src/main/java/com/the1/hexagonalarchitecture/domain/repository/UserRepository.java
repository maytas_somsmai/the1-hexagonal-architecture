package com.the1.hexagonalarchitecture.domain.repository;

import com.the1.hexagonalarchitecture.domain.User;

public interface UserRepository {

    User getUser(String userId, String randomString);

}
