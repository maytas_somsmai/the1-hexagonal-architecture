package com.the1.hexagonalarchitecture.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Location implements Serializable {

    private Location.DataObj data;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DataObj implements Serializable {

        private String address;

        private String zipCode;

    }

}
