package com.the1.hexagonalarchitecture.application.controller;

import com.the1.hexagonalarchitecture.application.response.UserInfoResponse;
import com.the1.hexagonalarchitecture.domain.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserInfoController {

    private final UserService userService;

    public UserInfoController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<UserInfoResponse> getUser(@RequestParam String userId) {
        return ResponseEntity.ok(userService.getUserInfo(userId));
    }

}
