package com.the1.hexagonalarchitecture.application.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoRequest {

    private String firstName;

    private String nickName;

    private String lastName;

    private String address;

    private String zipCode;

}
