package com.the1.hexagonalarchitecture.application.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoResponse {

    private String id;

    private String displayName;

    private String address;

    //make a custom lombok builder
    //use encapsulation for hide display name & address creation logic
    public static class UserInfoResponseBuilder {
        public UserInfoResponseBuilder displayName(String firstName, String lastName, String nickName) {
            this.displayName = String.format("%s %s [%s]", firstName, lastName, nickName);
            return this;
        }

        public UserInfoResponseBuilder address(String address, String zipCode) {
            this.address = String.format("%s, %s.", address, zipCode);
            return this;
        }
    }

}
