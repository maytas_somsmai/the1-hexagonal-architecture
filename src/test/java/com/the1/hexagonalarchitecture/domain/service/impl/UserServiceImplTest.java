package com.the1.hexagonalarchitecture.domain.service.impl;

import com.the1.hexagonalarchitecture.application.response.UserInfoResponse;
import com.the1.hexagonalarchitecture.domain.Location;
import com.the1.hexagonalarchitecture.domain.User;
import com.the1.hexagonalarchitecture.domain.mapper.UserMapper;
import com.the1.hexagonalarchitecture.domain.repository.LocationRepository;
import com.the1.hexagonalarchitecture.domain.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private LocationRepository locationRepository;

    @Mock
    private UserRepository userRepository;

    @Spy
    private UserMapper userMapper;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void getUserInfoTest() {
        when(locationRepository.getUserAddress(anyString(), anyString())).thenReturn(
                Location.builder()
                        .data(Location.DataObj.builder()
                                .address("address")
                                .zipCode("zipCode")
                                .build())
                        .build()
        );
        when(userRepository.getUser(anyString(), anyString())).thenReturn(
                User.builder()
                        .data(User.DataObj.builder()
                                .firstName("firstName")
                                .lastName("lastName")
                                .nickName("nickName")
                                .id("1")
                                .build())
                        .build()
        );
        UserInfoResponse result = userService.getUserInfo("1");
        assertEquals("1", result.getId());
        assertEquals("firstName lastName [nickName]", result.getDisplayName());
        assertEquals("address, zipCode.", result.getAddress());
    }
}