package com.the1.hexagonalarchitecture.application.controller;

import com.the1.hexagonalarchitecture.application.response.UserInfoResponse;
import com.the1.hexagonalarchitecture.domain.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserInfoController.class)
class UserInfoControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getUserTest() throws Exception {
        when(userService.getUserInfo("1")).thenReturn(
                new UserInfoResponse("1", "displayName", "address")
        );
        mockMvc.perform(get("/user")
                .param("userId", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.displayName").value("displayName"))
                .andExpect(jsonPath("$.address").value("address"));
    }

}