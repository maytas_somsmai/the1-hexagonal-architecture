# README #
### Setup guild ###
1. This project use java version 11.
2. I'm provided you a docker compose file to start Redis in your Docker. And map at port 6379 of your machine.
   ```
   docker-compose up -d
   ```
3. You can start with a jar (in target/hexagonal-architecture-0.0.1-SNAPSHOT.jar) file
   ```   
   java -jar target/hexagonal-architecture-0.0.1-SNAPSHOT.jar
   ```
   or with maven
   ```   
   mvn spring-boot:run
   ```
4. Test the api with swagger ui at http://localhost:8080/swagger-ui.html

### Conclusion ###
I'm using user id from request to be a redis key. So the number in displayName and address in the response will be random if they didn't cache yet. 